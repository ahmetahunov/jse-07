https://gitlab.com/ahmetahunov/jse-07
# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven 4.0

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.volnenko.school/ahmetahunov/jse-07.git
cd jse-07
mvn clean install
```

## run app
```bash
java -jar target/jse-1.0.7.jar
```
