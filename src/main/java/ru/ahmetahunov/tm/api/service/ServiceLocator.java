package ru.ahmetahunov.tm.api.service;

public interface ServiceLocator {

    public IProjectService getProjectService();

    public ITaskService getTaskService();

    public IUserService getUserService();

    public IStateService getStateService();

}
