package ru.ahmetahunov.tm.api.service;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import java.util.Collection;

public interface IStateService {

    public Collection<AbstractCommand> getCommands();

    public User getCurrentUser();

    public void setCurrentUser(User user);

    public String getUserId();

    public boolean isAllowedCommand(Role... roles);

    public AbstractCommand getCommand(String operation);

}
