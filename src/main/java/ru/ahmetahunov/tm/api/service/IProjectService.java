package ru.ahmetahunov.tm.api.service;

import ru.ahmetahunov.tm.entity.Project;
import java.util.List;

public interface IProjectService extends IAbstractService<Project> {

    public Project createNewProject(String projectName, String userId);

    public Project findOne(String projectName, String userId);

    public List<Project> findAll(String userId);

    public void removeAll(String userId);

}
