package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

public final class TaskDescriptionCommand extends AbstractCommand {

    public TaskDescriptionCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "task-description";
    }

    @Override
    public String getDescription() {
        return "Show selected task description.";
    }

    @Override
    public void execute() throws IOException {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[TASK-DESCRIPTION]");
        System.out.print("Please enter project name: ");
        String name = ConsoleUtil.readMessage().trim();
        final Project project = projectService.findOne(name, user.getId());
        final String projectId = (project == null) ? "" : project.getId();
        System.out.print("Please enter task name: ");
        name = ConsoleUtil.readMessage().trim();
        final Task task = taskService.findOne(name, projectId, user.getId());
        if (task == null) {
            System.out.println("Selected task does not exist.");
            return;
        }
        System.out.println(InfoUtil.getItemInfo(task));
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
