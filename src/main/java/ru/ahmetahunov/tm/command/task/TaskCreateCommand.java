package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

public final class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws IOException {
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[TASK CREATE]");
        System.out.print("Enter project name or press enter to skip: ");
        final Project project = getProject(user.getId());
        final String projectId = (project == null) ? "" : project.getId();
        final Task task = createNewTask(projectId, user.getId());
        if (task == null) System.out.println("Already exists.\n[FAILED]");
        else System.out.println("[OK]");
    }

    private Task createNewTask(final String projectId, final String userId) throws IOException {
        System.out.print("Please enter task name: ");
        final String name = ConsoleUtil.readMessage().trim();
        if (name.isEmpty()) {
            System.out.println("Name cannot be empty.");
            return null;
        }
        final Task task = serviceLocator.getTaskService().createNewTask(name, projectId, userId);
        if (task == null) return null;
        System.out.print("Please enter description: ");
        task.setDescription(ConsoleUtil.readMessage());
        System.out.print("Please enter start date(example: 01.01.2020): ");
        task.setStartDate(DateUtil.parseDate(ConsoleUtil.readMessage()));
        System.out.print("Please enter finish date(example: 01.01.2020): ");
        task.setFinishDate(DateUtil.parseDate(ConsoleUtil.readMessage()));
        return task;
    }

    private Project getProject(final String userId) throws IOException {
        final IProjectService projectService = serviceLocator.getProjectService();
        final String name = ConsoleUtil.readMessage().trim();
        final Project project = projectService.findOne(name, userId);
        if (project != null) return project;
        System.out.println(name + " is not available.");
        System.out.println("Do you want use another project?<y/n>");
        if ("y".equals(ConsoleUtil.readMessage()))
            return getProject(userId);
        return null;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}