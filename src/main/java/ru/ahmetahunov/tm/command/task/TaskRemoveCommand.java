package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import java.io.IOException;

public final class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws IOException {
        final IProjectService projectService = serviceLocator.getProjectService();
        final User user = serviceLocator.getStateService().getCurrentUser();
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[TASK REMOVE]");
        System.out.print("Enter project name or press enter to skip:");
        final String projectName = ConsoleUtil.readMessage().trim();
        final Project project = projectService.findOne(projectName, user.getId());
        final String projectId = (project == null) ? "" : project.getId();
        System.out.print("Enter task name: ");
        final String name = ConsoleUtil.readMessage().trim();
        final Task task = taskService.findOne(name, projectId, user.getId());
        if (task == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        taskService.remove(task);
        System.out.println("[OK]");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}