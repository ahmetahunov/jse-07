package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;

public final class TaskListCommand extends AbstractCommand {

    public TaskListCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all available tasks.";
    }

    @Override
    public void execute() {
        final ITaskService taskService = serviceLocator.getTaskService();
        final User user = serviceLocator.getStateService().getCurrentUser();
        int i = 1;
        System.out.println("[TASK LIST]");
        for (Task task : taskService.findAll(user.getId())) {
            System.out.println(String.format("%d. %s", i++, task.getName()));
        }
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}