package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import java.io.IOException;

public final class TaskMoveCommand extends AbstractCommand {

    public TaskMoveCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "task-move";
    }

    @Override
    public String getDescription() {
        return "Change project for task.";
    }

    @Override
    public void execute() throws IOException {
        final ITaskService taskService = serviceLocator.getTaskService();
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[TASK-MOVE]");
        Project project = getProject("Please enter project name: ", user.getId());
        final String projectId = (project == null) ? "" : project.getId();
        System.out.print("Please enter task name: ");
        final String taskName = ConsoleUtil.readMessage().trim();
        final Task task = taskService.findOne(taskName, projectId, user.getId());
        if (task == null) {
            System.out.println("Selected task does not exist.");
            return;
        }
        project = getProject("Please enter new project name: ", user.getId());
        if (project == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        task.setProjectId(project.getId());
        taskService.merge(task);
        System.out.println("[OK]");
    }

    private Project getProject(final String message, final String userId) throws IOException {
        final IProjectService projectService = serviceLocator.getProjectService();
        System.out.print(message);
        final String projectName = ConsoleUtil.readMessage().trim();
        return projectService.findOne(projectName, userId);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
