package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.DateUtil;

public final class TaskEditDateCommand extends AbstractCommand {

    public TaskEditDateCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getName() {
        return "task-edit-dates";
    }

    @Override
    public String getDescription() {
        return "Edit start and finish dates of selected task.";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[EDIT DATES]");
        System.out.print("Please enter project name: ");
        final String projectName = ConsoleUtil.readMessage().trim();
        final Project project = serviceLocator.getProjectService().findOne(projectName, user.getId());
        System.out.print("Please enter task name: ");
        final String taskName = ConsoleUtil.readMessage().trim();
        final String projectId = (project == null) ? "" : project.getId();
        final Task task = serviceLocator.getTaskService().findOne(taskName, projectId, user.getId());
        if (task == null) {
            System.out.println("Selected task does not exist.");
            return;
        }
        System.out.print("Please enter new start date: ");
        final String startDate = ConsoleUtil.readMessage().trim();
        System.out.print("Please enter new finish date: ");
        final String finishDate = ConsoleUtil.readMessage().trim();
        task.setStartDate(DateUtil.parseDate(startDate));
        task.setFinishDate(DateUtil.parseDate(finishDate));
        serviceLocator.getTaskService().merge(task);
        System.out.println("[OK]");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
