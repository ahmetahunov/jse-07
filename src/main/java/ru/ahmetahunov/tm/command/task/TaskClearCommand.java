package ru.ahmetahunov.tm.command.task;

import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;

public final class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all user's available tasks.";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getStateService().getCurrentUser();
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.removeAll(user.getId());
        System.out.println("[ALL TASKS REMOVED]");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}