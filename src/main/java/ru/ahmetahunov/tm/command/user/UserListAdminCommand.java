package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;

public final class UserListAdminCommand extends AbstractCommand {

    public UserListAdminCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "user-list";
    }

    @Override
    public String getDescription() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        System.out.println("[USER LIST]");
        int i = 1;
        for (User user : serviceLocator.getUserService().findAll()) {
            System.out.println(String.format("%d. %s", i++, user.getLogin()));
        }
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
