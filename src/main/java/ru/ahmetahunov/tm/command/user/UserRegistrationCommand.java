package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.PassUtil;

public final class UserRegistrationCommand extends AbstractCommand {

    public UserRegistrationCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "user-register";
    }

    @Override
    public String getDescription() {
        return "New user registration.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRATION]");
        System.out.print("Please enter login: ");
        final String login = ConsoleUtil.readMessage().trim();
        System.out.print("Please enter password: ");
        String password = ConsoleUtil.readMessage().trim();
        System.out.print("Please enter new password one more time: ");
        String repeatPass = ConsoleUtil.readMessage();
        if (password.isEmpty() || !password.equals(repeatPass)) {
            System.out.println("Passwords do not match!");
            return;
        }
        password = PassUtil.getHash(password);
        final User user = serviceLocator.getUserService().createNewUser(login, password);
        if (user == null) {
            System.out.println("This login already exists. Try one more time!");
            return;
        }
        System.out.println("[OK]");
    }

}
