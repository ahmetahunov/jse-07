package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.PassUtil;

public final class UserChangePassCommand extends AbstractCommand {

    public UserChangePassCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "change-pass";
    }

    @Override
    public String getDescription() {
        return "Change password.";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[CHANGE PASSWORD]");
        System.out.print("Please enter old password: ");
        String password = ConsoleUtil.readMessage();
        password = PassUtil.getHash(password);
        if (!user.getPassword().equals(password)) {
            System.out.println("Wrong password!");
            return;
        }
        System.out.print("Please enter new password: ");
        password = ConsoleUtil.readMessage().trim();
        System.out.print("Please enter new password one more time: ");
        final String repeatPass = ConsoleUtil.readMessage();
        if (password.isEmpty() || !password.equals(repeatPass)) {
            System.out.println("Passwords do not match!");
            return;
        }
        password = PassUtil.getHash(password);
        user.setPassword(password);
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
