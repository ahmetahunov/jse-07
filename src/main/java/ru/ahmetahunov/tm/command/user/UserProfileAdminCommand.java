package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.InfoUtil;

public final class UserProfileAdminCommand extends AbstractCommand {

    public UserProfileAdminCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "show-user-profile";
    }

    @Override
    public String getDescription() {
        return "Show selected user's profile.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER PROFILE]");
        System.out.print("Please enter user login: ");
        final String login = ConsoleUtil.readMessage().trim();
        final IUserService userService = serviceLocator.getUserService();
        final User user = userService.findUser(login);
        if (user == null) {
            System.out.println("Selected user does not exist.");
            return;
        }
        System.out.println(InfoUtil.getUserInfo(user));
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
