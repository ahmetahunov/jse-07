package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.PassUtil;

public final class UserChangePassAdminCommand extends AbstractCommand {

    public UserChangePassAdminCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "change-user-pass";
    }

    @Override
    public String getDescription() {
        return "Change selected user's password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE USER'S PASSWORD]");
        System.out.print("Please enter user's login: ");
        final String login = ConsoleUtil.readMessage().trim();
        final IUserService userService = serviceLocator.getUserService();
        final User user = userService.findUser(login);
        if (user == null) {
            System.out.println("Selected user does not exist.");
            return;
        }
        System.out.print("Please enter new password: ");
        String password = ConsoleUtil.readMessage().trim();
        System.out.print("Please enter new password one more time: ");
        final String repeatPass = ConsoleUtil.readMessage();
        if (password.isEmpty() || !password.equals(repeatPass)) {
            System.out.println("Passwords do not match!");
            return;
        }
        password = PassUtil.getHash(password);
        user.setPassword(password);
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
