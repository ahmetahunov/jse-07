package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.RoleUtil;
import java.io.IOException;

public final class UserChangeAccessAdminCommand extends AbstractCommand {

    public UserChangeAccessAdminCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "change-user-access";
    }

    @Override
    public String getDescription() {
        return "Change selected user's access.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE USER'S ACCESS RIGHTS]");
        System.out.print("Please enter user's login: ");
        final String login = ConsoleUtil.readMessage().trim();
        final IUserService userService = serviceLocator.getUserService();
        final User user = userService.findUser(login);
        if (user == null) {
            System.out.println("Selected user does not exist.");
            return;
        }
        final Role role = getRole();
        user.setRole(role);
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }

    private Role getRole() throws IOException {
        System.out.print("Please enter role<User/Administrator>: ");
        final String answer = ConsoleUtil.readMessage().trim();
        final Role role = RoleUtil.getRole(answer);
        if (role == null) {
            System.out.println("Unknown value.");
            return getRole();
        }
        return role;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
