package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.InfoUtil;

public final class UserProfileCommand extends AbstractCommand {

    public UserProfileCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "user-profile";
    }

    @Override
    public String getDescription() {
        return "Show user's profile information.";
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        final User user = serviceLocator.getStateService().getCurrentUser();
        if (user == null) return;
        System.out.println(InfoUtil.getUserInfo(user));
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
