package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.PassUtil;
import ru.ahmetahunov.tm.util.RoleUtil;

import java.io.IOException;

public final class UserRegistrationAdminCommand extends AbstractCommand {

    public UserRegistrationAdminCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getName() {
        return "user-register-admin";
    }

    @Override
    public String getDescription() {
        return "New user registration with role setting.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRATION]");
        System.out.print("Please enter login: ");
        final String login = ConsoleUtil.readMessage().trim();
        System.out.print("Please enter password: ");
        String password = ConsoleUtil.readMessage().trim();
        System.out.print("Please enter new password one more time: ");
        final String repeatPass = ConsoleUtil.readMessage();
        if (password.isEmpty() || !password.equals(repeatPass)) {
            System.out.println("Passwords do not match!");
            return;
        }
        password = PassUtil.getHash(password);
        final User user = serviceLocator.getUserService().createNewUser(login, password);
        if (user == null) {
            System.out.println("This login already exists. Try one more time!");
            return;
        }
        user.setRole(getRole());
        System.out.println("[OK]");
    }

    private Role getRole() throws IOException {
        System.out.print("Please enter role<User/Administrator>: ");
        String answer = ConsoleUtil.readMessage().trim();
        final Role role = RoleUtil.getRole(answer);
        if (role == null) {
            System.out.println("Unknown value.");
            return getRole();
        }
        return role;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
