package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.enumerated.Role;

public final class UserLogOutCommand extends AbstractCommand {

    public UserLogOutCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "log-out";
    }

    @Override
    public String getDescription() {
        return "Log out.";
    }

    @Override
    public void execute() {
        System.out.println("[LOG OUT]");
        serviceLocator.getStateService().setCurrentUser(null);
        System.out.println("Have a nice day!");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
