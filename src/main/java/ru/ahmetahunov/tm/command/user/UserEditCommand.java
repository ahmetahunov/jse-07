package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.PassUtil;
import java.io.IOException;

public final class UserEditCommand extends AbstractCommand {

    public UserEditCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user's information.";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[USER EDIT]");
        System.out.print("Please enter password: ");
        String password = ConsoleUtil.readMessage();
        password = PassUtil.getHash(password);
        if (!user.getPassword().equals(password)) {
            System.out.println("Wrong password!");
            return;
        }
        System.out.print("Please enter new login: ");
        final String login = getLogin();
        user.setLogin(login);
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }

    private String getLogin() throws IOException {
        final IUserService userService = serviceLocator.getUserService();
        String login = ConsoleUtil.readMessage().trim();
        User user = userService.findUser(login);
        while (user != null) {
            System.out.println("User with this login already exists!");
            System.out.print("Please enter login: ");
            login = ConsoleUtil.readMessage().trim();
            user = userService.findUser(login);
        }
        return login;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
