package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;

public final class UserRemoveAdminCommand extends AbstractCommand {

    public UserRemoveAdminCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected user and all his projects and tasks.";
    }

    @Override
    public void execute() throws Exception {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("[USER REMOVE]");
        System.out.print("Please enter user login: ");
        String login = ConsoleUtil.readMessage().trim();
        final User user = userService.findUser(login);
        if (user == null) {
            System.out.println("Selected user does not exist.");
            return;
        }
        taskService.removeAll(user.getId());
        projectService.removeAll(user.getId());
        userService.remove(user);
        System.out.println("[OK]");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
