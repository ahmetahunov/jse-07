package ru.ahmetahunov.tm.command.user;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.PassUtil;
import java.security.NoSuchAlgorithmException;

public final class UserLogInCommand extends AbstractCommand {

    public UserLogInCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "log-in";
    }

    @Override
    public String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOG IN]");
        System.out.print("Please enter login: ");
        final String login = ConsoleUtil.readMessage().trim();
        final User user = serviceLocator.getUserService().findUser(login);
        if (user == null) {
            System.out.println("Selected user does not exist.");
            return;
        }
        System.out.print("Please enter password: ");
        final String password = ConsoleUtil.readMessage().trim();
        if (!isAccepted(password, user.getPassword())) {
            System.out.println("Wrong password.");
            return;
        }
        serviceLocator.getStateService().setCurrentUser(user);
        System.out.println("Welcome, " + user.getLogin());
    }

    private boolean isAccepted(final String password, final String hash) throws NoSuchAlgorithmException {
        final String checkPassword = PassUtil.getHash(password);
        return hash.equals(checkPassword);
    }

}
