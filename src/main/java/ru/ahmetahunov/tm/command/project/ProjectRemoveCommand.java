package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.entity.Project;
import java.io.IOException;

public final class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws IOException {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[PROJECT REMOVE]");
        System.out.print("Please enter project name: ");
        final String projectName = ConsoleUtil.readMessage().trim();
        final Project removedProject = projectService.findOne(projectName, user.getId());
        if (removedProject == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        taskService.removeAllProjectTasks(removedProject.getId(), user.getId());
        projectService.remove(removedProject);
        System.out.println("[OK]");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}