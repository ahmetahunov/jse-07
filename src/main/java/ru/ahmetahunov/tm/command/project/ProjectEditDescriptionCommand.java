package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;

public final class ProjectEditDescriptionCommand extends AbstractCommand {

    public ProjectEditDescriptionCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getName() {
        return "project-edit-desc";
    }

    @Override
    public String getDescription() {
        return "Edit description of project";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[EDIT DESCRIPTION]");
        System.out.print("Please enter project name: ");
        final String projectName = ConsoleUtil.readMessage().trim();
        final Project project = serviceLocator.getProjectService().findOne(projectName, user.getId());
        if (project == null) {
            System.out.println("Selected project does not exist");
            return;
        }
        System.out.println("Please enter new description:");
        final String description = ConsoleUtil.readMessage();
        project.setDescription(description);
        serviceLocator.getProjectService().merge(project);
        System.out.println("[OK]");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
