package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

public final class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws IOException {
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[PROJECT CREATE]");
        final Project project = createNewProject(user.getId());
        if (project == null) System.out.println("Already exists.\n[FAILED]");
        else System.out.println("[OK]");
    }

    private Project createNewProject(final String userId) throws IOException {
        System.out.print("Please enter project name: ");
        final String name = ConsoleUtil.readMessage().trim();
        if (name.isEmpty()) {
            System.out.println("Name cannot be empty.");
            return null;
        }
        final Project project = serviceLocator.getProjectService().createNewProject(name, userId);
        if (project == null) return null;
        System.out.println("Please enter description:");
        project.setDescription(ConsoleUtil.readMessage());
        System.out.print("Please insert start date(example: 01.01.2020): ");
        project.setStartDate(DateUtil.parseDate(ConsoleUtil.readMessage()));
        System.out.print("Please enter finish date(example: 01.01.2020): ");
        project.setFinishDate(DateUtil.parseDate(ConsoleUtil.readMessage()));
        return project;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}