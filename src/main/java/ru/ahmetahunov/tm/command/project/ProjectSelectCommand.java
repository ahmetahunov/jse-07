package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import java.io.IOException;

public final class ProjectSelectCommand extends AbstractCommand {

    public ProjectSelectCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "project-select";
    }

    @Override
    public String getDescription() {
        return "Show selected project with tasks.";
    }

    @Override
    public void execute() throws IOException {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[PROJECT SELECT]");
        System.out.print("Please enter project name: ");
        final String name = ConsoleUtil.readMessage().trim();
        final Project project = projectService.findOne(name, user.getId());
        if (project == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        System.out.println(project.getName() + ":");
        int i = 1;
        for (Task task : taskService.findAll(project.getId(), user.getId())) {
            System.out.println(String.format("  %d. %s", i++, task.getName()));
        }
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
