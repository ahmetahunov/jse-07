package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.DateUtil;

public final class ProjectEditDatesCommand extends AbstractCommand {

    public ProjectEditDatesCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getName() {
        return "project-edit-dates";
    }

    @Override
    public String getDescription() {
        return "Edit start and finish dates in selected project.";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[EDIT DATES]");
        System.out.print("Please enter project name: ");
        final String projectName = ConsoleUtil.readMessage().trim();
        final Project project = serviceLocator.getProjectService().findOne(projectName, user.getId());
        if (project == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        System.out.print("Please enter new start date: ");
        final String startDate = ConsoleUtil.readMessage().trim();
        System.out.print("Please enter new finish date: ");
        final String finishDate = ConsoleUtil.readMessage().trim();
        project.setStartDate(DateUtil.parseDate(startDate));
        project.setFinishDate(DateUtil.parseDate(finishDate));
        serviceLocator.getProjectService().merge(project);
        System.out.println("[OK]");
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
