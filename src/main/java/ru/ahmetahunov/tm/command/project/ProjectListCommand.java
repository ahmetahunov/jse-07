package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;

public final class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all available projects";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        final User user = serviceLocator.getStateService().getCurrentUser();
        int i = 1;
        System.out.println("[PROJECT LIST]");
        for (Project project : projectService.findAll(user.getId())) {
            System.out.println(String.format("%d. %s", i++, project.getName()));
        }
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}