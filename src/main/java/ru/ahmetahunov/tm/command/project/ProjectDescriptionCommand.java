package ru.ahmetahunov.tm.command.project;

import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

public final class ProjectDescriptionCommand extends AbstractCommand {

    public ProjectDescriptionCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return false; }

    @Override
    public String getName() {
        return "project-description";
    }

    @Override
    public String getDescription() {
        return "Show selected project information.";
    }

    @Override
    public void execute() throws IOException {
        final IProjectService projectService = serviceLocator.getProjectService();
        final User user = serviceLocator.getStateService().getCurrentUser();
        System.out.println("[PROJECT-DESCRIPTION]");
        System.out.print("Please enter project name: ");
        final String projectName = ConsoleUtil.readMessage().trim();
        final Project project = projectService.findOne(projectName, user.getId());
        if (project == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        System.out.println(InfoUtil.getItemInfo(project));
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
