package ru.ahmetahunov.tm.command.system;

import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.enumerated.Role;

public final class ForbiddenInfoCommand extends AbstractCommand {

    public ForbiddenInfoCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getName() {
        return "forbidden";
    }

    @Override
    public String getDescription() {
        return "Notifies about low level access rights.";
    }

    @Override
    public void execute() {
        System.out.println("You don't have permission. Operation rejected");
    }

    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

}
