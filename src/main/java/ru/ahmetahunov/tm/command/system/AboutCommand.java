package ru.ahmetahunov.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    public AboutCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Show information about application.";
    }

    @Override
    public void execute() {
        System.out.println("TaskManager");
        System.out.println("build: " + Manifests.read("buildNumber"));
        System.out.println("Developed by: " + Manifests.read("developer"));
        System.out.println("email: " + Manifests.read("email"));
    }

}
