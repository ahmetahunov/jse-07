package ru.ahmetahunov.tm.command.system;

import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.enumerated.Role;

public final class UnknownInfoCommand extends AbstractCommand {

    public UnknownInfoCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getName() {
        return "unknown";
    }

    @Override
    public String getDescription() {
        return "Notifies about unknown operation.";
    }

    @Override
    public void execute() {
        System.out.println("Unknown operation. Please enter help for help.");
    }

    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

}
