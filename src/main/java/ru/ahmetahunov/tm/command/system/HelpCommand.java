package ru.ahmetahunov.tm.command.system;

import ru.ahmetahunov.tm.api.service.IStateService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    public HelpCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return true; }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all available commands.";
    }

    @Override
    public void execute() {
        final IStateService stateService = serviceLocator.getStateService();
        for (AbstractCommand command : stateService.getCommands()) {
            if (!command.isSecure() && !stateService.isAllowedCommand(command.getRoles())) continue;
            System.out.println(String.format("%-19s: %s", command.getName(), command.getDescription()));
        }
    }

}