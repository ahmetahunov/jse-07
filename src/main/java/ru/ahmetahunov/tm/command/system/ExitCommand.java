package ru.ahmetahunov.tm.command.system;

import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    public ExitCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean isSecure() { return true; }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from Task Manager.";
    }

    public void execute() {
        System.out.println("Have a nice day!");
    }

}