package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected final ServiceLocator serviceLocator;

    public AbstractCommand(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract boolean isSecure();

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public Role[] getRoles() {
        return null;
    }

}