package ru.ahmetahunov.tm.context;

import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.*;
import ru.ahmetahunov.tm.command.*;
import ru.ahmetahunov.tm.command.project.*;
import ru.ahmetahunov.tm.command.system.*;
import ru.ahmetahunov.tm.command.task.*;
import ru.ahmetahunov.tm.command.user.*;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.ConsoleUtil;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.repository.UserRepository;
import ru.ahmetahunov.tm.service.*;
import ru.ahmetahunov.tm.util.PassUtil;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

public final class Bootstrap implements ServiceLocator {

    private final Map<String, AbstractCommand> commands = new TreeMap<>();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserService userService = new UserService(userRepository);

    private final IStateService stateService = new StateService(commands);

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IStateService getStateService() { return stateService; }

    public void init() throws Exception {
        initCommands();
        createDefaultUsers();
        startCycle();
    }

    private void startCycle() throws Exception {
        System.out.println( "*** WELCOME TO TASK MANAGER ***" );
        String operation = "";
        while (!"exit".equals(operation)) {
            System.out.print("Please enter command: ");
            operation = ConsoleUtil.readMessage().trim().toLowerCase();
            final AbstractCommand command = stateService.getCommand(operation);
            if (command == null) continue;
            command.execute();
            System.out.println();
        }
        ConsoleUtil.close();
    }

    private void initCommands() {
        registry(new HelpCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new ProjectCreateCommand(this));
        registry(new ProjectEditDescriptionCommand(this));
        registry(new ProjectEditDatesCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectRemoveCommand(this));
        registry(new ProjectSelectCommand(this));
        registry(new ProjectDescriptionCommand(this));
        registry(new TaskClearCommand(this));
        registry(new TaskCreateCommand(this));
        registry(new TaskEditDescriptionCommand(this));
        registry(new TaskEditDateCommand(this));
        registry(new TaskRemoveCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskDescriptionCommand(this));
        registry(new TaskMoveCommand(this));
        registry(new ExitCommand(this));
        registry(new UserLogInCommand(this));
        registry(new UserLogOutCommand(this));
        registry(new UserRegistrationCommand(this));
        registry(new UserRegistrationAdminCommand(this));
        registry(new UserEditCommand(this));
        registry(new UserProfileCommand(this));
        registry(new UserChangePassCommand(this));
        registry(new UserChangeAccessAdminCommand(this));
        registry(new UserChangePassAdminCommand(this));
        registry(new UserRemoveAdminCommand(this));
        registry(new UserProfileAdminCommand(this));
        registry(new UserListAdminCommand(this));
        registry(new AboutCommand(this));
        registry(new ForbiddenInfoCommand(this));
        registry(new UnknownInfoCommand(this));
    }
    
    private void registry(final AbstractCommand command) {
        if (command == null) return;
        commands.put(command.getName(), command);
    }

    private void createDefaultUsers() throws NoSuchAlgorithmException {
        User user = new User();
        user.setLogin("user");
        user.setPassword(PassUtil.getHash("0000"));
        userService.persist(user);
        user = new User();
        user.setLogin("admin");
        user.setPassword(PassUtil.getHash("admin"));
        user.setRole(Role.ADMINISTRATOR);
        userService.persist(user);
    }

}
