package ru.ahmetahunov.tm;

import ru.ahmetahunov.tm.context.Bootstrap;

public final class Application {

    public static void main( final String[] args ) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
