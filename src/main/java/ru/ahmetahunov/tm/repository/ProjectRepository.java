package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.entity.Project;
import java.util.LinkedList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void removeAll(final String userId) {
        for (Project project : findAll(userId)) {
            remove(project.getId());
        }
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> projects = new LinkedList<>();
        for (Project project : findAll()) {
            if (project.getUserId().equals(userId)) projects.add(project);
        }
        return projects;
    }

    @Override
    public Project findOne(final String projectName, final String userId) {
        for (Project project : findAll()) {
            if (project.getUserId().equals(userId) && project.getName().equals(projectName))
                return project;
        }
        return null;
    }

}
