package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.entity.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findUser(final String login) {
        for (User user : findAll()) {
            if (user.getLogin().equalsIgnoreCase(login)) return user;
        }
        return null;
    }

    @Override
    public void removeAll() {
        collection.clear();
    }

}
