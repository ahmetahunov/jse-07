package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.entity.Task;
import java.util.LinkedList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeAll(final String userId) {
        for (Task task : findAll(userId))
            remove(task.getId());
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> tasks = new LinkedList<>();
        for (Task task : findAll()) {
            if (task.getUserId().equals(userId)) tasks.add(task);
        }
        return tasks;
    }

    @Override
    public List<Task> findAll(final String projectId, final String userId) {
        final List<Task> tasks = new LinkedList<>();
        for (Task task : findAll()) {
            if (task.getUserId().equals(userId) && task.getProjectId().equals(projectId))
                tasks.add(task);
        }
        return tasks;
    }

    @Override
    public Task findOne(final String taskName, final String projectId, final String userId) {
        for (Task task : findAll()) {
            if (task.getUserId().equals(userId)
                    && task.getProjectId().equals(projectId)
                    && task.getName().equals(taskName))
                return task;
        }
        return null;
    }

}
