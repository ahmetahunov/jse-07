package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.api.repository.IRepository;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    protected final Map<String, T> collection = new HashMap<>();

    @Override
    public T persist(final T item) throws IdCollisionException {
        if (collection.containsKey(item.getId()))
            throw new IdCollisionException();
        collection.put(item.getId(), item);
        return item;
    }

    @Override
    public T merge(final T item) {
        collection.put(item.getId(), item);
        return item;
    }

    @Override
    public boolean containsValue(final T item) {
        return collection.containsValue(item);
    }

    @Override
    public T remove(final String id) {
        return collection.remove(id);
    }

    @Override
    public Collection<T> findAll() {
        return collection.values();
    }

    @Override
    public T findOne(final String id) {
        return collection.get(id);
    }

}
