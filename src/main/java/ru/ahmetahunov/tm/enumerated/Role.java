package ru.ahmetahunov.tm.enumerated;

public enum Role {

    USER("User"),
    ADMINISTRATOR("Administrator");

    private final String displayName;

    Role(final String name) {
        this.displayName = name;
    }

    public String displayName() {
        return displayName;
    }

}
