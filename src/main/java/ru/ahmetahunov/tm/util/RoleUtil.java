package ru.ahmetahunov.tm.util;

import ru.ahmetahunov.tm.enumerated.Role;

public final class RoleUtil {

    public static Role getRole(final String role) {
        if ("user".equals(role.toLowerCase()))
            return Role.USER;
        if ("administrator".equals(role))
            return Role.ADMINISTRATOR;
        return null;
    }

}
