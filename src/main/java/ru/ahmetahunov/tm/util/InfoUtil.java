package ru.ahmetahunov.tm.util;

import ru.ahmetahunov.tm.api.other.IItem;
import ru.ahmetahunov.tm.entity.User;

public final class InfoUtil {

    public static String getItemInfo(final IItem entity) {
        final StringBuilder sb = new StringBuilder();
        sb.append(entity.getName());
        sb.append("\nDescription: ");
        sb.append(entity.getDescription());
        sb.append("\nStart date: ");
        String date = DateUtil.formatDate(entity.getStartDate());
        if (date.equals("01.01.1970"))
            date = "not set";
        sb.append(date);
        sb.append("\nFinish date: ");
        date = DateUtil.formatDate(entity.getFinishDate());
        if (date.equals("01.01.1970"))
            date = "not set";
        sb.append(date);
        return sb.toString();
    }

    public static String getUserInfo(final User user) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Login: ");
        sb.append(user.getLogin());
        sb.append("\nUser status: ");
        sb.append(user.getRole().displayName());
        sb.append("\nUser id: ");
        sb.append(user.getId());
        return sb.toString();
    }

}
