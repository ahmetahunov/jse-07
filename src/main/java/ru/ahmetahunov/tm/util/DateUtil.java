package ru.ahmetahunov.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {

    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    public static Date parseDate(final String date) {
        try {
            return dateFormatter.parse(date);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

    public static String formatDate(final Date date) {
        return dateFormatter.format(date);
    }

}
