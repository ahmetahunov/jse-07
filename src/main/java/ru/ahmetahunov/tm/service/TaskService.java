package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.entity.Task;
import java.util.LinkedList;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository repository = (ITaskRepository) super.getRepository();

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task createNewTask(final String taskName, final String projectId, final String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null) return null;
        if (isExist(taskName, projectId, userId)) return null;
        final Task task = new Task();
        task.setName(taskName);
        task.setProjectId(projectId);
        task.setUserId(userId);
        return persist(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> tasks = new LinkedList<>();
        if (userId == null || userId.isEmpty()) return tasks;
        return repository.findAll(userId);
    }

    @Override
    public List<Task> findAll(final String projectId, final String userId) {
        final List<Task> tasks = new LinkedList<>();
        if (projectId == null || projectId.isEmpty()) return tasks;
        if (userId == null || userId.isEmpty()) return tasks;
        return repository.findAll(projectId, userId);
    }

    @Override
    public Task findOne(final String name, final String projectId, final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null) return null;
        return repository.findOne(name, projectId, userId);
    }

    @Override
    public void removeAllProjectTasks(final String projectId, final String userId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        for (Task task : repository.findAll(userId)) {
            if (task.getProjectId().equals(projectId)) repository.remove(task.getId());
        }
    }

    @Override
    public void removeAll(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAll(userId);
    }

    private boolean isExist(final String taskName, final String projectId, final String userId) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        if (projectId == null) return false;
        final Task task = findOne(taskName, projectId, userId);
        if (task == null) return false;
        return true;
    }

}
