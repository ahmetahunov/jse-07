package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.entity.Project;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository repository = (IProjectRepository) super.getRepository();

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @Override
    public Project createNewProject(final String projectName, final String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (isExist(projectName, userId)) return null;
        final Project project = new Project();
        project.setName(projectName);
        project.setUserId(userId);
        return persist(project);
    }

    @Override
    public Project findOne(final String name, final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(name, userId);
    }

    @Override
    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return repository.findAll(userId);
    }

    @Override
    public void removeAll(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAll(userId);
    }

    private boolean isExist(final String projectName, final String userId) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        final Project project = findOne(projectName, userId);
        if (project == null) return false;
        return true;
    }

}
