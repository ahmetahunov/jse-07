package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.api.service.IStateService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import java.util.Collection;
import java.util.Map;

public final class StateService implements IStateService {

    private final Map<String, AbstractCommand> commands;

    private User currentUser = null;

    public StateService(final Map<String, AbstractCommand> commands) { this.commands = commands; }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(final User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public String getUserId() {
        if (currentUser == null) return null;
        return currentUser.getId();
    }

    /*
    * Метод проверяет права на выполнение команды.
    * Метод вызывается, если команда не безопасна.
    * Если в метод передается null, значит команда доступна только вне учетной записи.
    * Если в метод передается пустой массив, значит команда не доступна никому, кроме системы.
    * В остальных случаях, согласно полученному массиву ролей.
     */
    @Override
    public boolean isAllowedCommand(final Role... roles) {
        if (roles == null && currentUser == null) return true;
        if (currentUser == null || roles == null) return false;
        for (Role role : roles) {
            if (currentUser.getRole().equals(role)) return true;
        }
        return false;
    }

    @Override
    public AbstractCommand getCommand(final String operation) {
        if (operation == null || operation.isEmpty()) return null;
        final AbstractCommand command = commands.get(operation);
        if (command == null) return commands.get("unknown");
        if (command.isSecure()) return command;
        if (isAllowedCommand(command.getRoles())) return command;
        return commands.get("forbidden");
    }

}
