package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.api.service.IAbstractService;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.api.repository.IRepository;
import java.util.UUID;

public class AbstractService<T extends AbstractEntity> implements IAbstractService<T> {

    private final IRepository<T> repository;

    public AbstractService(final IRepository<T> repository) {
        this.repository = repository;
    }

    public IRepository<T> getRepository() {
        return repository;
    }

    @Override
    public T persist(final T item) {
        if (item == null) return null;
        if (item.getId() == null || item.getId().isEmpty()) return null;
        try {
            repository.persist(item);
            return item;
        } catch (IdCollisionException e) {
            item.setId(UUID.randomUUID().toString());
            return persist(item);
        }
    }

    @Override
    public T merge(final T item) {
        if (item == null) return null;
        if (item.getId() == null || item.getId().isEmpty()) return null;
        return repository.merge(item);
    }

    @Override
    public boolean containsValue(final T item) {
        if (item == null) return false;
        return repository.containsValue(item);
    }

    @Override
    public T remove(final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.remove(id);
    }

    @Override
    public T remove(final T item) {
        if (item == null) return null;
        if (item.getId() == null || item.getId().isEmpty()) return null;
        return repository.remove(item.getId());
    }

    @Override
    public T findOne(final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOne(id);
    }

}
