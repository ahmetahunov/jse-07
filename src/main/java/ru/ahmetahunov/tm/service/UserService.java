package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import java.util.Collection;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository repository = (IUserRepository) super.getRepository();

    public UserService(final IUserRepository repository) {
        super(repository);
    }

    @Override
    public User findUser(final String login) {
        if (login == null || login.isEmpty()) return null;
        return repository.findUser(login);
    }

    @Override
    public Collection<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User createNewUser(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (isExist(login)) return null;
        final User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        return persist(user);
    }

    private boolean isExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        final User user = findUser(login);
        if (user == null) return false;
        return true;
    }

}
